// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCkwBSSMfI1xRdPEQusl3Tln2lo_VtH9nc',
    authDomain: 'controle-money-tree.firebaseapp.com',
    databaseURL: 'https://controle-money-tree.firebaseio.com',
    projectId: 'controle-money-tree',
    storageBucket: 'controle-money-tree.appspot.com',
    messagingSenderId: '413713205554',
    appId: '1:413713205554:web:1298a57d0033c309476f2b',
    measurementId: 'G-7GG6PB3TWY'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
